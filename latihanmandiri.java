import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

class Person {
    protected String name;
    protected String address;
    protected String hobby;

    public Person(String name, String address, String hobby) {
        this.name = name;
        this.address = address;
        this.hobby = hobby;
    }

    public void identity() {
        System.out.println("Nama : " + name);
        System.out.println("Alamat : " + address);
    }

    public String getHobby() {
        return hobby;
    }
}

class Student extends Person {
    private final String nim;
    private Double SPP;
    private Double SKS;
    private Double modul;

    public Student(String name, String address, String hobby, String nim) {
        super(name, address, hobby);
        this.nim = nim;
    }


    public String getNim() {
        return nim;
    }

    @Override
    public void identity() {
        System.out.println();

        super.identity();
        System.out.println("NIM : " + getNim());
        System.out.println("Hobi : " + super.getHobby());

        NumberFormat format = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));

        System.out.println("Pembayaran SPP : " + format.format(this.SPP));
        System.out.println("Pembayaran SKS : " + format.format(this.SKS));
        System.out.println("Pembayaran Modul : " + format.format(this.modul));
    }

    public void setSPP(Double SPP) {
        this.SPP = SPP;
    }

    public void setSKS(Double SKS) {
        this.SKS = SKS;
    }

    public void setModul(Double modul) {
        this.modul = modul;
    }
}

class InheritMain {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan nama : ");
        String nama = scanner.nextLine();

        System.out.print("Masukkan alamat : ");
        String address = scanner.nextLine();

        System.out.print("Masukkan nim : ");
        String nim = scanner.nextLine();

        System.out.print("Masukkan hobby : ");
        String hobby = scanner.nextLine();

        Student mahasiswa = new Student(nama, address, hobby, nim);

        System.out.print("Masukkan pembayaran SPP : ");
        mahasiswa.setSPP(scanner.nextDouble());

        System.out.print("Masukkan pembayaran SKS : ");
        mahasiswa.setSKS(scanner.nextDouble());

        System.out.print("Masukkan pembayaran Modul : ");
        mahasiswa.setModul(scanner.nextDouble());

        mahasiswa.identity();
    }
}
